# Benedikt's dotfiles

My dotfiles for a more appealing and usable desktop experience with
i3, terminals, editors, and more.

![Screenshot of an example setup](preview.png "An example setup")  
*Wallpaper by Masashi Wakui*


## Table of Contents

* [Components](#components)
  + [Desktop](#desktop)
  + [CLI](#cli)
  + [Theming](#theming)
* [Installation](#installation)
  + [Applications](#applications)
  + [Dotfiles](#dotfiles)


## Components

This repository contains configuration and themes for the following
applications:

### Desktop

| | Name | Package | Links |
|-| ---- | ------- | ----- |
| **Window Manager** | i3-gaps | `i3-gaps` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/i3-gaps/)</sup> | [GitHub](https://github.com/Airblader/i3), [Docs](https://i3wm.org/docs/userguide.html)
| **Status Bar** | Polybar | `polybar` <sup>[AUR](https://aur.archlinux.org/packages/polybar/)</sup> | [GitHub](https://github.com/jaagr/polybar), [Docs](https://github.com/polybar/polybar/wiki)
| **Compositor** | picom | `picom` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/picom/)</sup> | [GitHub](https://github.com/yshui/picom)
| **Notification Daemon** | Dunst | `dunst` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/dunst/)</sup> | [GitHub](https://github.com/dunst-project/dunst), [Docs](https://github.com/dunst-project/dunst/blob/master/docs/dunst.pod)
| **Application Launcher** | Rofi | `rofi` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/rofi/)</sup> | [GitHub](https://github.com/davatorium/rofi), [Docs](https://github.com/davatorium/rofi/wiki)

### CLI

| | Name | Package | Links |
|-| ---- | ------- | ----- |
| **Shell** | fish | `fish` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/fish/)</sup> | [GitHub](https://github.com/fish-shell/fish-shell), [Docs](https://fishshell.com/docs/current/index.html)
| **Terminal** | kitty | `kitty` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/kitty/)</sup> | [GitHub](https://github.com/kovidgoyal/kitty), [Docs](https://sw.kovidgoyal.net/kitty/#contents)
| **File Manager** | ranger | `ranger` <sup>[Arch](https://www.archlinux.org/packages/community/any/ranger/)</sup> | [GitHub](https://github.com/ranger/ranger), [Docs](https://github.com/ranger/ranger/wiki)
| **Editor (Main)** | Neovim | `neovim` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/neovim/)</sup> | [GitHub](https://github.com/neovim/neovim), [Docs](https://github.com/neovim/neovim/wiki)
| **Editor (Alt.)** | Kakoune | `kakoune` <sup>[Arch](https://www.archlinux.org/packages/community/x86_64/kakoune/)</sup> | [GitHub](https://github.com/mawww/kakoune)

### Theming

| Name | Package | Links |
| ---- | ------- | ----- |
| Pywal | `python-pywal` <sup>[Arch](https://www.archlinux.org/packages/community/any/python-pywal/)</sup> &nbsp; \| &nbsp; `pywal` <sup>[PyPI](https://pypi.org/project/pywal/)</sup> | [GitHub](https://github.com/dylanaraps/pywal), [Docs](https://github.com/dylanaraps/pywal/wiki)


## Installation

### Applications

On Arch and derivatives, you may use the following to install all of the
packages listed above:

```shell
pacman -S i3-gaps picom dunst rofi fish kitty ranger vim kakoune python-pywal
```

```shell
yay -S polybar
```

### Dotfiles

1.  Clone the repository and run `./install`. This creates symlinks to the
    dotfiles in your home directory.

2.  When prompted, you may choose to run any of the included post-install
    tasks to set up dependencies:

    *   **Task 1:** Install script dependencies
        [[Source](https://gitlab.com/BVollmerhaus/scripts/-/blob/master/install-dependencies)]  
        ⓘ Installs all Python dependencies for the included
        [`scripts`](https://gitlab.com/BVollmerhaus/scripts) submodule.

    *   **Task 2:** Download Iosevka font
        [[Source](/post-install/install-iosevka)]  
        ⓘ Downloads the *Iosevka Term SS04* font used as the terminal
        font in kitty.

    *   **Task 3:** Download M+ Nerd Font
        [[Source](/post-install/install-mplus)]  
        ⓘ Downloads the *M+ 1m* font used in Polybar, Dunst, Rofi, and
        elsewhere.

    > You can, of course, also install these dependencies manually.
